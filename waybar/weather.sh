#!/usr/bin/env bash

# Get the weather
weather=$(curl "https://wttr.in?format=1")

# Remove the plus sign
echo ${weather/+/ }
