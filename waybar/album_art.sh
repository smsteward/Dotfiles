#!/usr/bin/env bash

# Check if music is playing, and if so, try to get album
album=$(playerctl --ignore-player=firefox,brave,chromium metadata xesam:album)
url=$(playerctl --ignore-player=firefox,brave,chromium metadata mpris:artUrl)
ext=${url##*.}

# If nothing playing, check for existing jpegs and delete, then exit
if [ $? -ne 0 ]
then
	if [ $(ls /tmp/*.jpeg | wc -l) -gt 0 ]
	then rm /tmp/*.jpeg
	fi
exit 0
fi

# Remove the empty jpeg file if nothing playing
if [ -f "/tmp/.jpeg" ]
then rm /tmp/.jpeg
fi

# Check if album artwork is already present
# If artwork exists, echo that and exit
if [ -f "/tmp/${album}.jpeg" ]
then echo "/tmp/${album}.jpeg"
exit 0
fi

# If artwork doesn't exist, delete any existing jpegs and fetch new artwork
# If using AudioTube which uses webp, convert using imagemagick
if [ $(ls /tmp/*.jpeg | wc -l) -gt 0 ]
then rm /tmp/*.jpeg
fi
if [ $ext = webp ]
then magick ${url:7} "/tmp/${album}.jpeg"
else
curl -s "${url}" --output "/tmp/${album}.jpeg"
fi
echo "/tmp/${album}.jpeg"
exit 0
