#!/usr/bin/env bash

# Check if music is playing, and if so, try to get album
url=$(playerctl --ignore-player=firefox,brave metadata mpris:artUrl)
ext=${url##*.}

# If nothing playing, delete existing image and move on
if [ $? -ne 0 ]
then rm "/tmp/.org.chromium.Chromium.${ext}"
exit 0
fi

# Check if album artwork is already present, and if so, echo that
if [ -f "/tmp/.org.chromium.Chromium.${ext}" ]
then echo "/tmp/.org.chromium.Chromium.${ext}"
exit 0
fi
