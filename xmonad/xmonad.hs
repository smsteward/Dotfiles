-- Stephen's XMonad configuration file
-- Always a work in progress, but trying to keep it simple

-- Haskell Language Imports
import System.Directory
import System.IO (hPutStrLn)
import System.Exit (exitSuccess)
import Data.Maybe (fromJust, isJust)
import Data.Monoid
import qualified Data.Map as M

-- XMonad Main Import
import XMonad

-- XMonad Actions Imports
import XMonad.Actions.CopyWindow (kill1)
import XMonad.Actions.CycleWS (Direction1D(..), moveTo, shiftTo, WSType(..), nextScreen, prevScreen)
import XMonad.Actions.RotSlaves (rotSlavesDown, rotAllDown)
import XMonad.Actions.WindowGo (runOrRaise)
import XMonad.Actions.WithAll (sinkAll, killAll)
import XMonad.Actions.GridSelect
import XMonad.Actions.MouseResize
import XMonad.Actions.Promote
import XMonad.Actions.PhysicalScreens
import qualified XMonad.Actions.Search as S

-- XMonad Hooks Imports
import XMonad.Hooks.EwmhDesktops
import XMonad.Hooks.ManageDocks (avoidStruts, docks, manageDocks, ToggleStruts(..))
import XMonad.Hooks.ManageHelpers (isFullscreen, doFullFloat, doCenterFloat)
import XMonad.Hooks.StatusBar
import XMonad.Hooks.StatusBar.PP
import XMonad.Hooks.SetWMName

-- XMonad Layout Imports
import XMonad.Layout.SimplestFloat
import XMonad.Layout.Spiral
import XMonad.Layout.ThreeColumns
import XMonad.Layout.Tabbed
import XMonad.Layout.ResizableTile

-- XMonad Layout Modifier Imports
import XMonad.Layout.LayoutModifier
import XMonad.Layout.LimitWindows (limitWindows, increaseLimit, decreaseLimit)
import XMonad.Layout.MultiToggle.Instances (StdTransformers(NBFULL, MIRROR, NOBORDERS))
import XMonad.Layout.MultiToggle (mkToggle, single, EOT(EOT), (??))
import XMonad.Layout.Renamed
import XMonad.Layout.Magnifier
import XMonad.Layout.ShowWName
import XMonad.Layout.Simplest
import XMonad.Layout.Spacing
import XMonad.Layout.SubLayouts
import XMonad.Layout.NoBorders
import XMonad.Layout.WindowArranger (windowArrange, WindowArrangerMsg(..))
import XMonad.Layout.WindowNavigation
import qualified XMonad.Layout.ToggleLayouts as T (toggleLayouts, ToggleLayout(Toggle))
import qualified XMonad.Layout.MultiToggle as MT (Toggle(..))

-- XMonad StackSet Imports
import qualified XMonad.StackSet as W

-- XMonad Utility Imports
import XMonad.Util.Dmenu
import XMonad.Util.Run (runProcessWithInput, safeSpawn, spawnPipe)
import XMonad.Util.SpawnOnce
import XMonad.Util.EZConfig (additionalKeysP)
import XMonad.Util.Loggers
import XMonad.Util.ClickableWorkspaces

-- Color Scheme Import
import Colors.DoomOne

-- Set font
myFont :: String
myFont = "xft:Ubuntu:regular:size=9:antialias=true:hinting=true"

-- Set mod key to Super key
myModMask :: KeyMask
myModMask = mod4Mask

-- Set window border width in pixels
myBorderWidth :: Dimension
myBorderWidth = 2

-- Set window border color
myNormColor :: String
myNormColor = colorBack

-- Set window focus color
myFocusColor :: String
myFocusColor = color15

-- Get window count
windowCount :: X (Maybe String)
windowCount = gets $ Just . show . length . W.integrate' . W.stack . W.workspace . W.current . windowset

-- Personal Variables
myTerminal :: String
myTerminal = "alacritty"

myBrowser :: String
myBrowser = "librewolf"

myChromium :: String
myChromium = "ungoogled-chromium"

myFileManager :: String
myFileManager = "pcmanfm"

myIde :: String
myIde = "pyenv/bin/spyder"

myEmail :: String
myEmail = "mailspring"

myGames :: String
myGames = "steam"

myTor :: String
myTor = ".tor-browser_en-US/Browser/start-tor-browser"

myMusic :: String
myMusic = "ytmdesktop"

-- Start hook to launch personal processes on startup
myStartupHook :: X ()
myStartupHook = do
    spawnOnce "lxsession &"
    spawnOnce "xinput set-prop 8 'libinput Natural Scrolling Enabled' 1"
    spawnOnce "redshift &"
    spawnOnce "nm-applet &"
    spawnOnce "volumeicon &"
    spawnOnce "xautolock -time 60 -locker 'systemctl suspend' &"
    spawnOnce "picom --experimental-backends &"
    spawnOnce "nitrogen --restore &"
    spawn ("trayer --edge top --align right --widthtype request --SetDockType true --SetPartialStrut true --monitor 1 --expand true --transparent true --alpha 0 " ++ colorTrayer ++ " --height 24")
    setWMName "LG3D"

-- Set spacing between windows
mySpacing :: Int -> l a -> XMonad.Layout.LayoutModifier.ModifiedLayout Spacing l a
mySpacing i = spacing i

-- Set tab colors
myTabTheme = def { fontName            = myFont,
                   activeColor         = color15,
                   inactiveColor       = color08,
                   activeBorderColor   = color15,
                   inactiveBorderColor = colorBack,
                   activeTextColor     = colorBack,
                   inactiveTextColor   = color16
                 }

-- Set layouts
tall     = renamed [Replace "tall"]
           $ smartBorders
           $ windowNavigation
           $ addTabs shrinkText myTabTheme
           $ subLayout [] (smartBorders Simplest)
           $ limitWindows 12
           $ mySpacing 6
           $ ResizableTall 1 (3/100) (1/2) []
fullscr  = renamed [Replace "magnify"]
           $ smartBorders
           $ windowNavigation
           $ addTabs shrinkText myTabTheme
           $ subLayout [] (smartBorders Simplest)
           $ magnifier
           $ limitWindows 12
           $ mySpacing 6
           $ ResizableTall 1 (3/100) (1/2) []
spirals  = renamed [Replace "spirals"]
           $ smartBorders
           $ windowNavigation
           $ addTabs shrinkText myTabTheme
           $ subLayout [] (smartBorders Simplest)
           $ mySpacing 6
           $ spiral (6/7)
threeCol = renamed [Replace "threeCol"]
           $ smartBorders
           $ windowNavigation
           $ addTabs shrinkText myTabTheme
           $ subLayout [] (smartBorders Simplest)
           $ limitWindows 7
           $ ThreeCol 1 (3/100) (1/2)
threeRow = renamed [Replace "threeRow"]
           $ smartBorders
           $ windowNavigation
           $ addTabs shrinkText myTabTheme
           $ subLayout [] (smartBorders Simplest)
           $ limitWindows 7
           $ Mirror
           $ ThreeCol 1 (3/100) (1/2)
floats   = renamed [Replace "floats"]
           $ smartBorders
           $ limitWindows 20 simplestFloat

-- Show workspace name when workspace changes
myShowWNameTheme :: SWNConfig
myShowWNameTheme = def { swn_font     = "xft:Ubuntu:bold:size=60",
                         swn_fade    = 1.0,
                         swn_bgcolor = "#1c1f24",
                         swn_color   = "#ffffff"
                       }

-- Define layout order when using M-<Tab>
myLayoutHook = avoidStruts $ mouseResize $ smartBorders $ windowArrange $ T.toggleLayouts floats
               $ mkToggle (NBFULL ?? NOBORDERS ?? EOT) myDefaultLayout
             where
               myDefaultLayout =     withBorder myBorderWidth tall
                                 ||| fullscr
                                 ||| spirals
                                 ||| threeCol
                                 ||| threeRow
                                 ||| floats

-- Set workspace names
myWorkspaces = [" dev ", " www ", " sys ", " doc ", " mus ", " vid ", " gfx ", " kvm "]
myWorkspaceIndices = M.fromList $ zipWith (,) myWorkspaces [1..]

-- Floating window rules
myManageHook :: XMonad.Query (Data.Monoid.Endo WindowSet)
myManageHook = composeAll
    [ className =? "confirm"                   --> doFloat,
      className =? "file_progress"             --> doFloat,
      className =? "dialog"                    --> doFloat,
      className =? "download"                  --> doFloat,
      className =? "error"                     --> doFloat,
      className =? "notification"              --> doFloat,
      className =? "pinentry"                  --> doFloat,
      className =? "pinentry-qt"               --> doFloat,
      className =? "splash"                    --> doFloat,
      className =? "toolbar"                   --> doFloat,
      isFullscreen                             --> doFullFloat,
      className =? "youtube-music-desktop-app" --> doShift (myWorkspaces !! 4),
      (className =? "librewolf" <&&> resource =? "Dialog") --> doFloat
    ]

-- Key bindings
myKeys :: [(String, X ())]
myKeys = [ ("M-C-r", spawn "xmonad --recompile"),
           ("M-S-r", spawn "xmonad --restart"),
           ("M-C-q", io exitSuccess),
           ("M-d", spawn "/usr/local/bin/dmenu_run"),
           ("M-<Return>", spawn (myTerminal)),
           ("M-b", spawn (myBrowser)),
           ("M-c", spawn (myChromium)),
           ("M-f", spawn (myFileManager)),
           ("M-p", spawn (myIde)),
           ("M-e", spawn (myEmail)),
           ("M-s", spawn (myGames)),
           ("M-t", spawn (myTor)),
           ("M-y", spawn (myMusic)),
           ("M-w", kill1),
           ("M-S-c", killAll),
           ("M-.", nextScreen),
           ("M-,", prevScreen),
           ("M-<Tab>", sendMessage NextLayout),
           ("M-j", windows W.focusDown),
           ("M-k", windows W.focusUp),
           ("M-m", windows W.focusMaster),
           ("M-S-m", promote),
           ("M-C-j", decWindowSpacing 4),
           ("M-C-k", incWindowSpacing 4),
           ("M-C-h", decScreenSpacing 4),
           ("M-C-l", incScreenSpacing 4),
           ("M-S-<Tab>", rotSlavesDown),
           ("M-C-<Tab>", rotAllDown),
           ("M-<Space>", sendMessage (MT.Toggle NBFULL) >> sendMessage ToggleStruts),
           ("<XF86AudioRaiseVolume>", spawn "pactl set-sink-volume @DEFAULT_SINK@ +5%"),
           ("<XF86AudioLowerVolume>", spawn "pactl set-sink-volume @DEFAULT_SINK@ -5%"),
           ("<XF86AudioMute>", spawn "pactl set-sink-mute @DEFAULT_SINK@ toggle"),
           ("M-C-1", viewScreen def 0),
           ("M-C-2", viewScreen def 1)
         ]

-- Define XMobarPP settings
myPP = def {
            -- Current workspace
            ppCurrent = xmobarColor color03 "" . wrap "[" "]",
            -- Visible but not current workspace
            ppVisible = xmobarColor color06 "" . wrap "{" "}",
            -- Hidden workspace
            ppHidden = xmobarColor color04 "" . wrap "<" ">",
            -- Hidden workspaces (no windows)
            ppHiddenNoWindows = xmobarColor color05 "",
            -- Title of active window
            ppTitle = xmobarColor color16 "" . shorten 60,
            -- Separator character
            ppSep =  "<fc=" ++ color09 ++ "> <fn=1>|</fn> </fc>",
            -- Urgent workspace
            ppUrgent = xmobarColor color02 "" . wrap "!" "!",
            -- Adding # of windows on current workspace to the bar
            ppExtras  = [windowCount],
            -- order of things in xmobar
            ppOrder  = \(ws:l:t:ex) -> [ws,l]++ex++[t]
           }

-- Define custom key to toggle struts
toggleStrutsKey :: XConfig Layout -> (KeyMask, KeySym)
toggleStrutsKey XConfig{ modMask = m } = (m, xK_a)

-- Initialize XMobar
xmobarNoTray = statusBarProp "xmobar -x 0 $HOME/.config/xmobar/doom-one-xmobarrc-notray" (pure myPP)
xmobarTray   = statusBarProp "xmobar -x 1 $HOME/.config/xmobar/doom-one-xmobarrc-tray" (pure myPP)

-- Initialize main
main :: IO ()
main = do
    xmonad . withEasySB (xmobarNoTray <> xmobarTray) toggleStrutsKey . ewmhFullscreen . ewmh . docks $ def {
        manageHook         = myManageHook <+> manageDocks <+> (isFullscreen --> doFullFloat),
        modMask            = myModMask,
        terminal           = myTerminal,
        startupHook        = myStartupHook,
        layoutHook         = showWName' myShowWNameTheme $ myLayoutHook,
        workspaces         = myWorkspaces,
        borderWidth        = myBorderWidth,
        normalBorderColor  = myNormColor,
        focusedBorderColor = myFocusColor
                                                                                     } `additionalKeysP` myKeys
