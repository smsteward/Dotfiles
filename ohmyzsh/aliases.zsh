# Personal aliases
alias sn='sudo nano'
alias sv='sudo vim'
alias up='sudo pacman -Syu'
alias upp='paru -Syu'
alias ar='pacman -Qtdq | sudo pacman -Rns -'
alias pc='protonvpn-cli c'
alias pd='protonvpn-cli d'
alias zup='omz update'
alias cup='checkupdates || echo "No pacman updates available" && paru -Qua || echo "No AUR updates available"'
alias rw='killall waybar && hyprctl dispatch exec waybar'
alias rh='killall hyprpaper && hyprctl dispatch exec hyprpaper'
alias rs='killall wlsunset && hyprctl dispatch exec "wlsunset -t 3000 -l 35.9 -L -78.9"'
alias aenv='source ~/pyenv/bin/activate'
alias mpv='mpv --profile=sdef'
alias bw='cd ~/Documents/Waybar && rm -rf build && meson build && ninja -C build && sleep 2 && killall waybar && sleep 2 && hyprctl dispatch exec waybar'

# System maintenance
alias po='systemctl poweroff'
alias rb='systemctl reboot'
alias sp='swaylock -f -c 000000 && sleep 1 && hyprctl dispatch dpms off && sleep 1 && systemctl suspend'
alias ..='cd ..'
alias h='cd ~'
alias ls='eza --all --long --header --git'
alias grep='rg'
alias cat='bat'

# Function to cd and list contents all at once
function cl() {
	DIR="$*";
	if [ $# -lt 1 ]; then
		DIR=$HOME;
	fi;
	builtin cd "${DIR}" && ls
}

# Function to call swayimg without SWAYSOCK error
function img() {
	swayimg "$1" 2> /dev/null
}

# Alias to git using AUR ssh keys
alias git-AUR='GIT_SSH_COMMAND="ssh -i ~/.ssh/aur" git'
